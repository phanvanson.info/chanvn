const express = require('express')
const morgan = require('morgan');
const helmet = require('helmet') ;
const cors = require('cors');
const dotenv = require('dotenv') ;
const rfs = require("rotating-file-stream");
const path = require('path');
const http = require('http');

const app = express();

const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

const timer = ms => new Promise( res => setTimeout(res, ms));

io.on('connection',  (socket) => {
    console.log('a user connected');
    socket.on('chat message', async (msg) => {
        console.log('message: ' + msg);
        
        for (let index = 0; index < 10; index++) {
            console.log("wait 3 seconds")
            await  timer(3000) ;
            socket.emit('chat message', 'du ma no ngoi hat mot minh ' + index );
            
        }
        
        
    });
});
 
io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
      console.log('user disconnected');
    });
});
   
const router = require('./chan/vn/src/routers/routes') ;
const connectDatabase = require('./chan/vn/src/configs/db.config')
dotenv.config();

connectDatabase()

const port = process.env.PORT || 8001;
isProduction = process.env.NODE_ENV === "production" ;



app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });


app.use('/test' ,router );

var listener = server.listen(port, () =>{
    console.log('Server is listening on port: ' + listener.address().port );
})

module.exports = app;